#!/usr/bin/python
# -*- coding: UTF-8 -*-
# Copyright (c) 2014,OpenThings Projects.

# 通过WiFi自动扫描进行模块参数配置。

import sys
import os
import time

#扫描所有的AP，形成AP列表。

#对符合前缀的AP(ESP***)进行尝试连接（测试网段192.168.1.*所有IP地址）。

#如果连接成功，通过IP连接无线写入配置参数。

#重启成功配置的该模块。

#测试配置成功的模块是否有效。

